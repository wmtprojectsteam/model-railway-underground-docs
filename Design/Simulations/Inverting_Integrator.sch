<Qucs Schematic 0.0.19>
<Properties>
  <View=-60,-120,1558,789,1,0,0>
  <Grid=10,10,1>
  <DataSet=Inverting_Integrator.dat>
  <DataDisplay=Inverting_Integrator.dpl>
  <OpenDisplay=1>
  <Script=Inverting_Integrator.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <OpAmp OP1 1 520 300 -23 51 1 0 "1e6" 1 "15 V" 0>
  <GND * 1 450 340 0 0 0 0>
  <GND * 1 270 340 0 0 0 0>
  <Vrect V2 1 270 310 18 -26 0 1 "3 V" 1 "0.7 ms" 1 "0.3 ms" 1 "1 ns" 0 "1 ns" 0 "0 ns" 0>
  <R R1 1 360 280 -38 -55 0 0 "10 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <C C1 1 500 150 -30 9 0 2 "10 nF" 1 "" 0 "neutral" 0>
  <.TR TR1 1 690 450 0 65 0 0 "lin" 1 "0" 1 "10 ms" 1 "10000" 0 "Trapezoidal" 0 "2" 0 "1 ns" 0 "1e-16" 0 "150" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "26.85" 0 "1e-3" 0 "1e-6" 0 "1" 0 "CroutLU" 0 "no" 0 "yes" 0 "0" 0>
</Components>
<Wires>
  <450 320 490 320 "" 0 0 0 "">
  <450 320 450 340 "" 0 0 0 "">
  <560 300 580 300 "" 0 0 0 "">
  <410 280 490 280 "" 0 0 0 "">
  <410 150 410 280 "" 0 0 0 "">
  <410 150 470 150 "" 0 0 0 "">
  <580 150 580 300 "" 0 0 0 "">
  <530 150 580 150 "" 0 0 0 "">
  <390 280 410 280 "" 0 0 0 "">
  <270 280 330 280 "" 0 0 0 "">
  <560 300 560 300 "IntegratorOutput" 590 270 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
