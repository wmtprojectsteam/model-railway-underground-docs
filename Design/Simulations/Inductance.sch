<Qucs Schematic 0.0.19>
<Properties>
  <View=-89,-45,1010,800,1.17897,0,0>
  <Grid=10,10,1>
  <DataSet=Inductance.dat>
  <DataDisplay=Inductance.dpl>
  <OpenDisplay=1>
  <Script=Inductance.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND * 1 260 500 0 0 0 0>
  <R R1 1 340 440 -30 -68 0 2 "1 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <Vac V1 1 260 470 18 -26 0 1 "3 V" 1 "1 GHz" 0 "0" 0 "0" 0>
</Components>
<Wires>
  <260 440 310 440 "" 0 0 0 "">
  <370 440 410 440 "" 0 0 0 "">
  <370 440 370 440 "Vin" 400 500 0 "">
</Wires>
<Diagrams>
  <Rect 192 297 803 293 3 #c0c0c0 1 00 0 0 0.01 0.1 1 -3 1 0.250002 1 -1 0.5 1 315 0 225 "" "" "">
	<"VOut.Vt@time" #0000ff 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
</Paintings>
