<Qucs Schematic 0.0.19>
<Properties>
  <View=-60,-60,1558,596,1.331,45,0>
  <Grid=10,10,1>
  <DataSet=Non-inverting_Integrator.dat>
  <DataDisplay=Non-inverting_Integrator.dpl>
  <OpenDisplay=1>
  <Script=Non-inverting_Integrator.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND * 1 590 340 0 0 0 0>
  <OpAmp OP1 1 690 300 -23 -89 0 0 "1e6" 1 "15 V" 0>
  <C C2 1 590 310 -101 -12 0 1 "100 nF" 1 "" 0 "neutral" 0>
  <.TR TR1 1 780 320 0 65 0 0 "lin" 1 "0" 1 "100ms" 1 "10000" 0 "Trapezoidal" 0 "2" 0 "1 ns" 0 "1e-16" 0 "150" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "26.85" 0 "1e-3" 0 "1e-6" 0 "1" 0 "CroutLU" 0 "no" 0 "yes" 0 "0" 0>
  <R R1 1 450 280 -38 -55 0 0 "10 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <GND * 1 300 370 0 0 0 0>
  <R R6 1 540 280 -14 -54 0 0 "100 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <Vrect V2 1 300 310 -106 -37 0 1 "3 V" 1 "900 us" 1 "100 us" 1 "1 ns" 0 "1 ns" 0 "0 ns" 0>
  <C C3 1 630 150 -26 17 0 0 "100 nF" 1 "" 0 "neutral" 0>
</Components>
<Wires>
  <570 280 590 280 "" 0 0 0 "">
  <730 150 730 300 "" 0 0 0 "">
  <660 150 730 150 "" 0 0 0 "">
  <650 320 660 320 "" 0 0 0 "">
  <650 320 650 370 "" 0 0 0 "">
  <650 370 750 370 "" 0 0 0 "">
  <750 300 750 370 "" 0 0 0 "">
  <730 300 750 300 "" 0 0 0 "">
  <590 280 660 280 "" 0 0 0 "">
  <510 150 510 280 "" 0 0 0 "">
  <510 150 600 150 "" 0 0 0 "">
  <480 280 510 280 "" 0 0 0 "">
  <300 280 420 280 "" 0 0 0 "">
  <300 340 300 370 "" 0 0 0 "">
  <750 300 750 300 "IntegratorOutput" 780 270 0 "">
  <510 150 510 150 "IntegratorInput" 350 110 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
